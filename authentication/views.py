from django.shortcuts import render
from django.http import JsonResponse
import requests
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

# Create your views here.

@login_required
def index(req):
	return render(req, 'index.html')

@login_required
def get_data(req):
	# print('called')
	key = req.GET['key']
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + key

	response = requests.get(url)
	response_json = response.json();

	return JsonResponse(response_json)

class sign_up(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
