from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class AuthenticationTest(TestCase):
    
    def test_url_kosong(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 302)
    
    def test_ga_ada_url(self):
        response = Client().get('/ga_ada/')
        self.assertEqual(response.status_code, 404)
    
    def test_ada_url_login(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_ada_url_logout(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)
    
    def test_ada_url_register(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_pake_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

class FunctionalTest(StaticLiveServerTestCase):

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		options.add_argument('--no-sandbox')
		options.add_argument('--disable-dev-shm-usage')
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=options)
		self.browser.get(self.live_server_url)

	def tearDown(self):
		self.browser.quit()

	def test_judul(self):
		browser = self.browser
		self.assertIn("Book Kezia", browser.title)
