$(document).ready(() => {
	$('#button').click(function() {
		var key = $("#search").val()
		// alert(key);
		$.ajax({
			method: 'GET',
			url: '/getData?key=' + key,
			success: function(response) {	// response -> bebas				
				let html = "";
				if (response.totalItems != 0) {
					for (let i = 0; i < response.items.length; i++) {
						html += '<tr>';
						html += '<th rowspan="2" scope="row">' + (i+1) + '</th>';

						html += '<td rowspan="2">';
						let volume_info = response.items[i].volumeInfo;
						html += '<img src="';
						if (volume_info.imageLinks != undefined) {
							html += volume_info.imageLinks.thumbnail;
						} else {
							html += 'https://upload.wikimedia.org/wikipedia/commons/6/6c/No_image_3x4.svg';
						}
						html += '" class="card-img">';
						html += '</td>';

						html += '<td>' + volume_info.title + '</td>';

						html += '<td>';
						if (volume_info.authors != undefined) {
							html += '<ul>';
							let authors_length = volume_info.authors.length;
							for (let j = 0; j < authors_length; j++) {
								html += '<li>' + volume_info.authors[j] + '</li>';
							}
							html += '</ul>';
						} else {
							html += "-";
						}
						html += '</td>';
						html += '</tr>';

						html += '<tr>';
						html += '<td colspan="2">';
						let search_info = response.items[i].searchInfo;
						if (search_info != undefined) {
							html += search_info.textSnippet;
						} else {
							html += "-";
						}
						html += '</td>';
						html += '</tr>';
					}
				} else {
					html += '<tr>';
					html += '<td colspan="4">';
					html += '<h4 align="center">';
					html += 'Sorry, book not found';
					html += '</h4>';
					html += '</td>';
					html += '</tr>';
				}
				$('tbody').empty();
				$(html).appendTo($('tbody'));
			}
		})
	})
})
