from django.test import TestCase, Client

# Create your tests here.
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

class FunctionalTest(StaticLiveServerTestCase):

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		options.add_argument('--no-sandbox')
		options.add_argument('--disable-dev-shm-usage')
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=options)
		self.browser.get(self.live_server_url)

	def tearDown(self):
		self.browser.quit()

	def test_judul(self):
		browser = self.browser
		self.assertIn("Book Kezia", browser.title)
